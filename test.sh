PLUGIN_VERSION=0.5.0
jars="org/github/flytreeleft/nexus3-keycloak-plugin/$PLUGIN_VERSION/nexus3-keycloak-plugin-$PLUGIN_VERSION.jar"

for jar in $(echo $jars | sed 's/ /\n/g'); do
    mkdir -p $install_dir/system/$(dirname $jar)
    cp ~/.m2/repository/$jar $install_dir/system/$jar
done

 mkdir -p /opt/sonatype/nexus/system/org/github/flytreeleft/nexus3-keycloak-plugin/0.5.0/nexus3-keycloak-plugin-0.5.0.jar