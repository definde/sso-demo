PLUGIN_VERSION=0.5.0
KEYCLOAK_PLUGIN_VERSION=0.5.0
KEYCLOAK_PLUGIN=/org/github/flytreeleft/nexus3-keycloak-plugin/${KEYCLOAK_PLUGIN_VERSION}/nexus3-keycloak-plugin-${KEYCLOAK_PLUGIN_VERSION}
jars="org/github/flytreeleft/nexus3-keycloak-plugin/$PLUGIN_VERSION/nexus3-keycloak-plugin-$PLUGIN_VERSION.jar"

for jar in $(echo $jars | sed 's/ /\n/g'); do
    echo "COPY FILE"
    mkdir -p /opt/sonatype/nexus/system/$(dirname $jar)
    cp /opt/sonatype/nexus3-keycloak-plugin-$PLUGIN_VERSION.jar /opt/sonatype/nexus/system/$jar
done

echo "reference\:file\:${KEYCLOAK_PLUGIN}.jar = 200" >> /opt/sonatype/nexus/etc/karaf/startup.properties